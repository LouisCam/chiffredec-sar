from tkinter.messagebox import *
from tkinter import * 

if __name__ == "__main__":
    frame = Tk()
    
frame.title("Le chiffre de César")
depart = "0.0"

def traduction_cryptage():
    entre = textarea.get("0.0", END)
    cle = int(insertkey.get("0.0", END))
    sortie = []
    for char in entre :
        ascii = ord(char)
        ascii = ascii + cle
        char = chr(ascii)
        char = list(char)
        sortie = sortie + char
    showinfo('Traduction', "".join(sortie))

def traduction_decryptage():
    entre = textarea.get("0.0", END)
    cle = int(insertkey.get("0.0", END))
    sortie = []
    for char in entre :
        ascii = ord(char)
        ascii = ascii - cle
        char = chr(ascii)
        char = list(char)
        sortie = sortie + char
    showinfo('Traduction', "".join(sortie))

insertkey = Text(frame, wrap='word', height=1, width=20)
insertkey.insert(depart, "Saisir une cle")
insertkey.grid(row=0, columnspan=2)

textarea = Text(frame, wrap='word')
textarea.insert(depart, "Saisir un texte")
textarea.grid(row=1, columnspan=2, )

Cryptage_button = Button(frame, text='Cryptage', command=traduction_cryptage)
Decryptage_button = Button(frame, text='Decryptage', command=traduction_decryptage)

Decryptage_button.grid(row=2, column=0)
Cryptage_button.grid(row=2, column=1)

frame.mainloop()